#!/bin/bash
source .venv/bin/activate
python -m flake8 src tests --max-line-length=120 --show-source --statistics
