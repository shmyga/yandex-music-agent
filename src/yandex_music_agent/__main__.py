import asyncio

from yandex_music_agent import main


def __main__():
    asyncio.run(main.run())


if __name__ == "__main__":
    __main__()
