import os

from asynctest import TestCase

from yandex_music_agent import auth
from yandex_music_agent.api import YandexMusicApi
from yandex_music_agent.main import Credentials


class ApiTester(TestCase):

    @classmethod
    def setUpClass(cls):
        credentials = Credentials(os.path.dirname(__file__))
        cookie = auth.resolve_cookie(credentials.login, credentials.password)
        cls.api = YandexMusicApi(cookie)

    async def test_favorite_artists(self):
        artists = await self.api.get_favorite_artists()
        self.assertLess(0, len(artists))
        artist = artists[0]
        self.assertIsNotNone(artist.id)
        self.assertIsNotNone(artist.title)
        # self.assertIsNotNone(artist.avatar)

    async def test_artist_albums(self):
        albums = await self.api.get_artist_albums(168862)
        self.assertLess(0, len(albums))
        album = albums[0]
        self.assertIsNotNone(album.id)
        self.assertIsNotNone(album.title)
        self.assertIsNotNone(album.year)
        self.assertIsNotNone(album.cover)

    async def test_album_tracks(self):
        tracks = await self.api.get_album_tracks(2166448)
        self.assertLess(0, len(tracks))
        track = tracks[0]
        self.assertIsNotNone(track.id)
        self.assertIsNotNone(track.album_id)
        self.assertIsNotNone(track.title)
        self.assertIsNotNone(track.num)

    async def test_track_url(self):
        url = await self.api.get_track_url(2166448, 19272898)
        self.assertIsNotNone(url)

    async def test_account(self):
        account = await self.api.get_account()
        self.assertIsNotNone(account)
