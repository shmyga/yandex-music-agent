import os
from unittest import TestCase

from yandex_music_agent import auth
from yandex_music_agent.auth import AuthException
from yandex_music_agent.data import YandexCookie
from yandex_music_agent.main import Credentials


class AuthTester(TestCase):

    def test_auth(self):
        credentials = Credentials(os.path.dirname(__file__))
        cookie = auth.resolve_cookie(credentials.login, credentials.password)
        self.assertTrue(cookie)
        self.assertIsNotNone(cookie.uid)
        self.assertIsNotNone(cookie.login)
        result = auth.validate_cookie(cookie)
        self.assertTrue(result)

    def test_auth_fail(self):
        with self.assertRaises(AuthException):
            auth.resolve_cookie("test", "test")

    def test_invalid_cookie(self):
        result = auth.validate_cookie(YandexCookie())
        self.assertFalse(result)
