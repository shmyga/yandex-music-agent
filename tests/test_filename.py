from typing import Tuple
from unittest import TestCase

from yandex_music_agent.data import Artist, Album, Track
from yandex_music_agent.filename import normalize_base, MusicFilenameBuilder


class FilenameTester(TestCase):

    @classmethod
    def build_default_data(cls) -> Tuple[Artist, Album, Track]:
        return Artist(1, "Artist", None), \
               Album(1, "Album", 2000, None), \
               Track(1, 1, "Track", 1),

    def test(self):
        builder = MusicFilenameBuilder()
        filename = builder.build_track_filename(*self.build_default_data())
        self.assertEqual("Artist/2000 - Album/01. Track.mp3", filename)

    def test_template(self):
        builder = MusicFilenameBuilder("{track.title} ({artist.title} - {album.title})")
        filename = builder.build_track_filename(*self.build_default_data())
        self.assertEqual("Track (Artist - Album)", filename)

    def test_normalize(self):
        artist = Artist(1, "\\Artist/", None)
        album = Album(1, "!Album?", 2000, None)
        track = Track(1, 1, ";Track,", 1)

        builder = MusicFilenameBuilder()
        filename = builder.build_track_filename(artist, album, track)
        self.assertEqual("_Artist_/2000 - _Album_/01. _Track_.mp3", filename)

        builder = MusicFilenameBuilder(normalizer=normalize_base)
        filename = builder.build_track_filename(artist, album, track)
        self.assertEqual("\\Artist-/2000 - !Album?/01. ;Track,.mp3", filename)
